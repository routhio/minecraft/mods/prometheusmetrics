package mcrewson.prometheusmetrics;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Summary;
import io.prometheus.client.exporter.HTTPServer;
import io.prometheus.client.hotspot.DefaultExports;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldServer;
import net.minecraft.world.gen.ChunkProviderServer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

@Mod(modid = "prometheusmetrics", 
     name = "Prometheus Metrics", 
     version = "@MOD_VERSION@", 
     useMetadata = true,
     acceptableRemoteVersions = "*", 
     acceptedMinecraftVersions = "[1.12.2,)")
public class PrometheusMetrics
{
    private int httpPort = 1234;

    public static long tickTime = 20;
    public static long lastTickTime;

    @Mod.EventHandler
    public void preInit (FMLPreInitializationEvent event)
    {
        Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        config.load();
        String GENERAL = Configuration.CATEGORY_GENERAL;

        httpPort = config.get(GENERAL, "httpPort", httpPort, "Port to run the http server on").getInt();
        config.save();
    }

    @Mod.EventHandler
    public void init (FMLInitializationEvent event)
        throws IOException
    {
        HTTPServer server = new HTTPServer(httpPort);
        DefaultExports.initialize();

        lastTickTime = System.nanoTime();
        MinecraftForge.EVENT_BUS.register(new TickHandler());
    }

    private static class TickHandler
    {
        static final Counter ticks = Counter.build().name("ticks").help("Server ticks").register();
        static final Summary ticktimesum = Summary.build().name("tick_time").help("Server tick time")
                                           .quantile(0.5, 0.5)
                                           .quantile(0.9, 0.1)
                                           .quantile(0.95, 0.01)
                                           .register();

        static final Gauge ticksPerSecond = Gauge.build().name("mc_tps").help("Server ticks per second").register();
        static final Gauge entitiesLoaded = Gauge.build().name("mc_entities").labelNames("world").help("Count of entities, per dimension").register();
        static final Gauge tileEntitiesLoaded = Gauge.build().name("mc_tile_entities").labelNames("world").help("Count of tile entities, per dimension").register();
        static final Gauge chunksLoaded = Gauge.build().name("mc_loaded_chunks").labelNames("world").help("Count of loaded chunks, per dimension").register();

        static final Gauge players = Gauge.build().name("mc_players_total").labelNames("state").help("Total online and max players").register();

        private int serverTicks = 0;

        @SubscribeEvent
        public void tick (TickEvent.ServerTickEvent tick)
        {
            serverTicks++;
            if (tick.phase == TickEvent.Phase.START)
            {
                long time = System.nanoTime();
                long thisTickTime = time - lastTickTime;
                lastTickTime = time;
                tickTime = (tickTime * 19 + thisTickTime) / 20;
                ticktimesum.observe(tickTime);

                double tps = TimeUnit.SECONDS.toNanos(1) / (double)tickTime;
                if (tps > 20)
                {
                    tps = 20;
                }
                ticksPerSecond.set(tps);
                ticks.inc();
            }

            switch (serverTicks % 60)
            {
                case 27:
                    setWorldMetrics();
                    break;
                case 47:
                    setPlayerMetrics();
                    break;
            }
        }

        private void setWorldMetrics ()
        {
            for (int dimension : DimensionManager.getIDs())
            {
                WorldServer world = DimensionManager.getWorld(dimension);
                ChunkProviderServer provider = world.getChunkProvider();
                DimensionType providerType = DimensionManager.getProviderType(dimension);

                String dimensionName = "";
                if (providerType != null)
                {
                    dimensionName = providerType.getName();
                }

                int worldEntities = world.loadedEntityList.size();
                int worldTileEntities = world.loadedTileEntityList.size();
                int worldChunks = provider.getLoadedChunkCount();

                entitiesLoaded.labels(dimensionName).set(worldEntities);
                tileEntitiesLoaded.labels(dimensionName).set(worldTileEntities);
                chunksLoaded.labels(dimensionName).set(worldChunks);
            }
        }

        private void setPlayerMetrics ()
        {
            MinecraftServer mcserver = FMLCommonHandler.instance().getMinecraftServerInstance();
            int currentPlayers = mcserver.getPlayerList().getCurrentPlayerCount();
            int maxPlayers = mcserver.getPlayerList().getMaxPlayers();

            players.labels("online").set(currentPlayers);
            players.labels("max").set(maxPlayers);
        }

    }

}

// THE END
